import { FaChevronLeft, FaChevronRight } from "react-icons/fa"
import styles from "./styles.module.scss"

const CPagination = ({ children }) => {
  return (
    <div
      style={{
        display: "flex",
        marginTop: "16px",
        gap: "8px",
        alignItems: "center",
      }}
    >
      {children}
    </div>
  )
}

CPagination.Left = ({ handleClick, disabled }) => (
  <button disabled={disabled} onClick={handleClick}>
    <FaChevronLeft />
  </button>
)
CPagination.Right = ({ handleClick, disabled }) => (
  <button disabled={disabled} onClick={handleClick}>
    <FaChevronRight />
  </button>
)
CPagination.Pages = ({ currentPage, setPage, totalPages }) => {
  const handlePageClick = (pageNumber) => {
    setPage(pageNumber)
  }

  const renderedPages = Array.from({ length: totalPages }, (_, index) => (
    <span
      className={index + 1 === currentPage ? styles.current : styles.noCurrent}
      onClick={() => handlePageClick(index + 1)}
      key={index}
    >
      {index + 1}
    </span>
  ))
  return (
    <div className={styles.pagination}>
      <div className={styles.pageNumbers}>{renderedPages}</div>
    </div>
  )
}

export default CPagination
