import { Controller } from "react-hook-form"
import { NumericFormat } from "react-number-format"

import cls from "./input.module.scss"

const NewInput = ({
  placeholder,
  className = "",
  name = "name",
  errors = {},
  control = {},
  required,
  disabled,
  size,
  labelText,
  defaultValue,
  id,
  customOnChange = () => {},
  ...restProps
}) => {
  return (
    <div className={`${cls.inputBox} ${className}`}>
      {labelText && (
        <label htmlFor={id}>
          {labelText} {required ? "*" : ""}
        </label>
      )}

      <Controller
        control={control}
        name={name}
        defaultValue={defaultValue}
        rules={{ required }}
        render={({ field: { value, onChange, name } }) => {
          return (
            <>
              <NumericFormat
                value={value}
                placeholder={placeholder}
                thousandsGroupStyle="thousand"
                thousandSeparator=" "
                decimalSeparator="."
                displayType="input"
                autoComplete="off"
                allowNegative={false}
                name={name}
                id={id}
                onChange={(e) => {
                  onChange(e.target.value)
                  customOnChange(e)
                }}
                className={`${cls.input} 
              // ${errors[name] ? cls.error : ""}
              ${disabled ? cls.disabled : ""}
              ${cls[size]}
              `}
                {...restProps}
              />
            </>
          )
        }}
      />
    </div>
  )
}

export default NewInput
