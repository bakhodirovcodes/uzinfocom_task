import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"

import Card from "./components/card"
import Arrow from "/src/assets/img/arrow.png"
import { cartData as staticCartData } from "./data"
import Details from "./components/details"
import styles from "./styles.module.scss"

const MainPage = () => {
  const dbName = "shoppingCartDB"
  const storeName = "cartData"

  const [cartItems, setCartItems] = useState([])
  const [cardData, setCardData] = useState([])
  const [fetchDataTrigger, setFetchDataTrigger] = useState(0)

  const { register, control, setValue, handleSubmit, reset } = useForm({
    name: "",
    card_number: "",
    date: "",
    cvv: "",
  })

  const openDB = () => {
    return new Promise((resolve, reject) => {
      const request = indexedDB.open(dbName, 1)

      request.onupgradeneeded = (event) => {
        const db = event.target.result

        if (!db.objectStoreNames.contains(storeName)) {
          db.createObjectStore(storeName, { keyPath: "id" })
        }
      }

      request.onsuccess = (event) => {
        resolve(event.target.result)
      }

      request.onerror = (event) => {
        reject(event.target.error)
      }
    })
  }

  const saveToDB = async (data) => {
    try {
      const db = await openDB()
      const transaction = db.transaction(storeName, "readwrite")
      const objectStore = transaction.objectStore(storeName)

      const itemsToSave = data.map((item) => {
        if (!item.shipping) {
          return { ...item, shipping: 4 } // Set your default shipping value here
        }
        return item
      })
      itemsToSave.forEach((item) => {
        objectStore.put(item)
      })

      transaction.oncomplete = () => {
        console.log("Data saved to IndexedDB")
      }
    } catch (error) {
      console.error("Error saving to IndexedDB:", error)
    }
  }

  const getFromDB = async () => {
    try {
      const db = await openDB()
      const transaction = db.transaction(storeName, "readonly")
      const objectStore = transaction.objectStore(storeName)

      const request = objectStore.getAll()

      return new Promise((resolve, reject) => {
        request.onsuccess = () => {
          resolve(request.result)
        }

        request.onerror = (event) => {
          reject(event.target.error)
        }
      })
    } catch (error) {
      console.error("Error getting data from IndexedDB:", error)
    }
  }
  useEffect(() => {
    const fetchData = async () => {
      try {
        const dataFromDB = await getFromDB()

        if (dataFromDB?.length === 0) {
          setCartItems(staticCartData)
          await saveToDB(staticCartData)
          staticCartData.forEach(({ id, count, price }) => {
            updateTotalPrice(id, count, price)
          })
        } else {
          setCartItems(dataFromDB)
        }
      } catch (error) {
        console.error("Error fetching data:", error)
      }
    }

    fetchData()
  }, [fetchDataTrigger])

  const handleDelete = async (id) => {
    try {
      const db = await openDB()
      const transaction = db.transaction(storeName, "readwrite")
      const objectStore = transaction.objectStore(storeName)

      objectStore.delete(id)

      await new Promise((resolve) => {
        transaction.oncomplete = () => {
          console.log("Data deleted from IndexedDB")
          resolve()
        }
      })

      setFetchDataTrigger((prevTrigger) => prevTrigger + 1)
    } catch (error) {
      console.error("Error deleting data from IndexedDB:", error)
    }

    setCartItems((prevItems) => prevItems.filter((item) => item.id !== id))
    setCardData((prevCardData) => prevCardData.filter((card) => card.id !== id))
  }

  const updateTotalPrice = (id, newCount, newPrice) => {
    setCardData((prevCardData) => {
      const existingCardIndex = prevCardData.findIndex((card) => card.id === id)
      if (existingCardIndex !== -1) {
        return prevCardData.map((card) =>
          card.id === id ? { ...card, count: newCount, price: newPrice } : card
        )
      } else {
        return [...prevCardData, { id, count: newCount, price: newPrice }]
      }
    })
  }

  const onSubmit = (value) => {
    alert(
      `Name: ${value?.name} , Card Number: ${value?.card_number}, Expiration date: ${value?.date}, CVV: ${value?.cvv} `
    )

    const resetValue = {
      name: "",
      card_number: "",
      date: "",
      cvv: "",
    }
    reset(resetValue)
  }

  return (
    <div className={styles.container}>
      <div className={styles.carts}>
        <div className={styles.header}>
          <img src={Arrow} alt="arrow" />
          <h4 className={styles.headerTitle}>Shopping Continue</h4>
        </div>

        <div className={styles.line}></div>

        <div className={styles.cartInfo}>
          <h4 className={styles.cartTitle}>Shopping cart</h4>
          <p className={styles.cartText}>
            You have {cartItems?.length} item in your cart
          </p>
        </div>

        <div className={styles.cards}>
          {cartItems?.map((el) => (
            <Card
              price={el.price}
              key={el.id}
              id={el.id}
              img={el.img}
              title={el.title}
              text={el.text}
              onDelete={handleDelete}
              updateTotalPrice={updateTotalPrice}
            />
          ))}
        </div>
      </div>
      <div className={styles.details}>
        <Details
          register={register}
          control={control}
          setValue={setValue}
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          changedData={cardData}
        />
      </div>
    </div>
  )
}

export default MainPage
