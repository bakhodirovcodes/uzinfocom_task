import IMG from "../../assets/img/img1.png"
import IMG1 from "../../assets/img/img2.png"
import IMG2 from "../../assets/img/img3.png"

export const cartData = [
  {
    id: 1,
    img: IMG,
    title: "Italy Pizza",
    text: "Extra cheese and toping",
    price: 681,
  },
  {
    id: 2,
    img: IMG1,
    title: "Combo Plate",
    text: "Extra cheese and toping",
    price: 681,
  },
  {
    id: 3,
    img: IMG2,
    title: "Spanish Rice",
    text: "Extra cheese and toping",
    price: 681,
  },
]
