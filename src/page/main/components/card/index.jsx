import { useState } from "react"

import UP from "/src/assets/icons/up.svg"
import Down from "/src/assets/icons/down.svg"
import TRASH from "/src/assets/img/trash.png"
import styles from "./styles.module.scss"

const Card = ({ id, img, title, text, onDelete, updateTotalPrice }) => {
  const [count, setCount] = useState(1)

  const handleCountChange = (newCount) => {
    setCount(newCount)
    updateTotalPrice(id, newCount, newCount * 681)
  }

  const handleUpClick = () => {
    handleCountChange(count + 1)
  }

  const handleDownClick = () => {
    const newCount = Math.max(1, count - 1)
    handleCountChange(newCount)
  }

  const handleTrashClick = () => {
    onDelete(id)
  }

  return (
    <div className={styles.container}>
      <img src={img} alt="cartImg" />
      <div className={styles.info}>
        <h4 className={styles.title}>{title}</h4>
        <p className={styles.text}>{text}</p>
      </div>
      <div className={styles.counter}>
        <span>{count}</span>
        <div className={styles.btns}>
          <img src={UP} alt="up" onClick={handleUpClick} />
          <img src={Down} alt="down" onClick={handleDownClick} />
        </div>
      </div>
      <p className={styles.price}>${count * 681}</p>

      <img
        src={TRASH}
        alt="trash"
        className={styles.trash}
        onClick={handleTrashClick}
      />
    </div>
  )
}

export default Card
