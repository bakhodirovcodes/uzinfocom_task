import { useEffect, useState } from "react"

import CARDIMG from "../../../../assets/img/carImg.png"
import Master from "../../../../assets/img/cards/master.png"
import Visa from "../../../../assets/img/cards/visa.png"
import Pay from "../../../../assets/img/cards/pay.png"
import InputPrice from "../../../../components/CElements/Input/InputPrice/InputPrice"
import NewInput from "../../../../components/CElements/Input/InputPrice/NewInpu"
import { cartData as staticCartData } from "../../data"
import RIGHT from "../../../../assets/img/Right.png"
import styles from "./styles.module.scss"

const Details = ({
  register,
  control,
  setValue,
  handleSubmit,
  onSubmit,
  changedData,
}) => {
  const dbName = "shoppingCartDB"
  const storeName = "cartData"
  const [cartData, setCardData] = useState([])
  const [totalPrices, setTotalPrices] = useState(0)

  const openDB = () => {
    return new Promise((resolve, reject) => {
      const request = indexedDB.open(dbName, 1)

      request.onupgradeneeded = (event) => {
        const db = event.target.result

        if (!db.objectStoreNames.contains(storeName)) {
          db.createObjectStore(storeName, { keyPath: "id" })
        }
      }

      request.onsuccess = (event) => {
        resolve(event.target.result)
      }

      request.onerror = (event) => {
        reject(event.target.error)
      }
    })
  }

  const getFromDB = async () => {
    try {
      const db = await openDB()
      const transaction = db.transaction(storeName, "readonly")
      const objectStore = transaction.objectStore(storeName)

      const request = objectStore.getAll()

      return new Promise((resolve, reject) => {
        request.onsuccess = () => {
          resolve(request.result)
        }

        request.onerror = (event) => {
          reject(event.target.error)
        }
      })
    } catch (error) {
      console.error("Error getting data from IndexedDB:", error)
    }
  }

  const fetchAndCalculateTotalPrices = async () => {
    try {
      const dataFromDB = await getFromDB()
      setCardData(dataFromDB)

      const updatedData = dataFromDB.map((originalItem) => {
        const changedItem = changedData.find(
          (item) => item.id === originalItem.id
        )

        return {
          ...originalItem,
          price: changedItem?.price || originalItem.price,
        }
      })

      // Calculate total prices only if changedData is not empty
      const calculatedTotalPrices = updatedData.reduce((total, item) => {
        const count = item.count || 1 // Default to 1 if count is undefined
        return total + item?.price * count
      }, 0)

      setTotalPrices(calculatedTotalPrices) // Update the state with total prices
    } catch (error) {
      console.error("Error fetching or calculating data:", error)
    }
  }
  console.log("total", totalPrices)

  useEffect(() => {
    fetchAndCalculateTotalPrices()
  }, [changedData])

  const handleDateChange = (e) => {
    const input = e.target.value.replace(/\D/g, "") // Remove non-numeric characters
    const formattedInput = input.replace(/(\d{2})(\d{0,2})/, "$1/$2")
    const maxLength = 5 // Maximum length for "MM/YY"
    setValue("date", formattedInput.slice(0, maxLength))
  }

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h4 className={styles.title}>Card Details</h4>
        <img src={CARDIMG} alt="cardImg" />
      </div>
      <div className={styles.cardTypes}>
        <p className={styles.typeText}>Card type</p>
        <div className={styles.cards}>
          <img src={Master} alt="master" />
          <img src={Visa} alt="visa" />
          <img src={Pay} alt="pay" />
          <div className={styles.seeAll}>
            <p className={styles.seeAllText}>See all</p>
          </div>
        </div>
      </div>

      <form onSubmit={handleSubmit(onSubmit)} className={styles.forms}>
        <p className={styles.label}>Name on card</p>
        <input type="text" placeholder="Name" {...register(`name`)} />
        <p className={styles.label}>Card Number</p>
        <InputPrice
          control={control}
          name="card_number"
          placeholder="1111 2222 3333 4444"
          maxLength={20}
        />
        <div className={styles.extraInfo}>
          <div className={styles.date}>
            <p className={styles.label}>Expiration date</p>
            <input
              type="text"
              placeholder="mm/yy"
              {...register(`date`)}
              onChange={handleDateChange}
            />
          </div>
          <div className={styles.date}>
            <p className={styles.label}>CVV</p>
            <NewInput
              control={control}
              name="cvv"
              placeholder="123"
              maxLength={3}
            />
          </div>
        </div>

        <div className={styles.line}></div>

        <div className={styles.cardCostInfo}>
          <ul>
            <li>Subtotal</li>
            <li>Shipping</li>
            <li>Total (Tax incl.)</li>
          </ul>
          <ul>
            <li>${totalPrices}</li>
            <li>${cartData?.[0]?.shipping ? cartData?.[0]?.shipping : 4}</li>
            <li>${totalPrices + 4}</li>
          </ul>
        </div>

        <button type="submit">
          ${totalPrices + 4}{" "}
          <span>
            Checkout <img src={RIGHT} alt="right" />
          </span>
        </button>
      </form>
    </div>
  )
}

export default Details
